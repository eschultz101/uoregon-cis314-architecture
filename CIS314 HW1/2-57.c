//Eric Schultz 2-57

#include <stdio.h>

void show_double(double x);
void show_long(long x);
void show_short(short x);

typedef unsigned char *byte_pointer;

int main(void) {

    short s = 1;
    long l = 1;
    double d = 1;
    show_short(s);
    show_long(l);
    show_double(d);

    return 0;
}

void show_bytes(byte_pointer start, int len)
{
     int i;
     for (i = 0; i < len; i++)
           printf(" %.2x", start[i]);
     printf("\n");
}

void show_short(short x)
{
     show_bytes((byte_pointer) &x, sizeof(short));
}

void show_long(long x)
{
     show_bytes((byte_pointer) &x, sizeof(long));
}

void show_double(double x)
{
     show_bytes((byte_pointer) &x, sizeof(double));
}

