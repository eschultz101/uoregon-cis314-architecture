//Eric Schultz 2-60

#include <stdio.h>

unsigned replace_byte(unsigned x, int i, unsigned char b);

int main (void) {

    unsigned a;
    unsigned x = 0x12345678;
    int i = 3;
    unsigned char b = 0xCD;

    a = replace_byte(x, i, b);

    printf("0x%X", a);

    return 0;
}

unsigned replace_byte(unsigned x, int i, unsigned char b) {

    unsigned newUns;
    unsigned mask = 0x000000FF;

    mask = mask << ((i - 1) * 8);
    newUns = x & ~mask;
    mask = 0;
    mask = mask | b;
    mask = mask << ((i - 1) * 8);
    newUns = newUns | mask;

    return newUns;
}
