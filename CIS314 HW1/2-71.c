//Eric Schultz 2-71

/*A. the (bytenum << 3) part of the code is incorrect. Simply word >> bytenum is all
that is needed to get at the correct byte within the word. bytenum << 3 will simply
shift the bits in the 32 bit representation of the integer bytenum, leading to a nonsensical value. */

//B. Correct code is:

#include <stdio.h>

int x_byte(unsigned testWord, int bytenum);

int main(void) {

    unsigned testWord = 0x12345678;
    int bytenum = 3;
    int result;

    result = x_byte(testWord, bytenum);

    printf("%d", result);

    return 0;
}

int x_byte(unsigned testWord, int bytenum) {

    int result;
    unsigned char mask = 0xFF;

    testWord = testWord >> (bytenum * 8);
    result = testWord & mask;

    return result;
}
