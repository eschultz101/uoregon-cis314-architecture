//Eric Schultz 2-72

/* A. The if statement always returns true because the size_t is an unsigned integer.
In arithmetic in C, if one variable is unsigned, it changes the others to unsigned as
well. This means the answer can never be negative, and the if statement will always be
true.*/

// B. The conditional should be written as: if(sizeof(val) <= maxbytes).

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void copy_int(int val, void* buf, int maxbytes);

int main(void) {

    int val = 1234;
    void* buf = malloc(sizeof(val) - 1); //this will make val too large for buf.
    int maxbytes = sizeof(buf);

    copy_int(val, buf, maxbytes);

    return 0;
}

void copy_int(int val, void* buf, int maxbytes) {

    if(sizeof(val) <= maxbytes) {
        memcpy(buf, (void*)&val, sizeof(val));
        printf("Value copied");
    } else {
        printf("Value not copied");
    }

    return;
}
