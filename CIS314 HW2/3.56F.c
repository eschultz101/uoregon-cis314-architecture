//Eric Schultz
//CIS314 HW2
//10/28/14

int loop(int, int);

int main(void) {

    int result, x = 0xFFFF, n = 0x0001;

    result = loop(x, n);

    printf("Result = %d\n", result);

    return 0;
}

int loop(int x, int n) {

    int result = -1;
    int mask;

    for (mask = 1; mask &= x; mask = mask << n) {
        result ^= mask;
    }

    return result;
}
