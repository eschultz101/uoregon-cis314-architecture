//Eric Schultz
//CIS314 HW2
//10/28/14

#include <stdio.h>

#define M 3

void transpose(int A[][M]);

int main(void) {

    int i = 0, j = 0;
    int A[M][M] = {{1, 2, 3},
                   {4, 5, 6},
                   {7, 8, 9}};

    transpose(A);

    while(i < M) {
        while(j < M) {
            printf("%d ", A[i][j]);
            j++;
        }
        printf("\n");
        i++;
        j = 0;
    }


    return 0;
}

void transpose(int A[M][M]) {
	int i = 0, j = 0, t;

	while (i < M) {
		while (j < i) {
			t = *(*(A + i) + j);
			*(*(A + i) + j) = *(*(A + j) + i);
			*(*(A + j) + i) = t;
			j++;
		}
		i++;
		j = 0;
	}
	return;
}
