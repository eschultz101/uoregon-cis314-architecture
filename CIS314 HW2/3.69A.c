//Eric Schultz
//CIS314 HW2
//10/28/14

#include <stdio.h>
#include <stdlib.h>

typedef struct tree_ptr {
    long val;
    struct tree_ptr* left;
    struct tree_ptr* right;
}ELE;

long trace(ELE*, int);

int main(void) {

    int i = 0, height;
    ELE* tp = (ELE*)malloc(sizeof(ELE));
    tp->left = NULL;
    tp->right = NULL;

    height = trace(tp, i);

    printf("Height of tree tp is %d\n", height);

    return 0;
}
/* This function computes the height of the tree tp */
long trace (ELE* tp, int i) {

	if(tp) {
        i++;
		trace(tp->left, i);
	}

	return i;
}
