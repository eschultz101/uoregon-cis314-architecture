//Eric Schultz
//CIS 314
//10/28/14

#include <stdio.h>
#include <stdlib.h>

#define MAX_INTS 100

int input(int array[]);
void ary_sort(int array[], int num);

int main(void) {

    int array[MAX_INTS];
    int num, i;

    num = input(array);

    ary_sort(array, num);

    for(i = 0; i < num; i++) {
        printf("array[%d] = %d\n", i, array[i]);
    }

    return 0;
}

int input(int array[]) {

    int i = 0, num;

    printf("Enter number of integers for array: ");
    scanf("%d", &num);

    //array = (int*)malloc(num * sizeof(int));

    printf("Enter numbers for array: ");
    for (i = 0; i < num; i++) {
        scanf("%d", &array[i]);
    }

    return num;
}

void ary_sort(int array[], int num) {

    //Select sort
    int current, walker, sIndex, temp;

    for(current = 0; current < num - 1; current++) {
        sIndex = current;
        for(walker = current; walker < num; walker++) {
            if(array[walker] < array[sIndex])
                sIndex = walker;
        }
        temp = array[current];
        array[current] = array[sIndex];
        array[sIndex] = temp;
    }

    return;
}

