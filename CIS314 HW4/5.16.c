//Eric Schultz
//CIS314
//5.16

//Inner4 procedure test, unrolledx4

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define VECTOR_SIZE 100

typedef long long data_t;
typedef data_t* vec_ptr;

void inner4_unrolled(vec_ptr u, vec_ptr v, data_t *dest);
int vec_length(vec_ptr v);
data_t * get_vec_start(vec_ptr v);

int main(void) {

    data_t arr1[VECTOR_SIZE];
    data_t arr2[VECTOR_SIZE];
    vec_ptr v = arr1;
    vec_ptr u = arr2;
    data_t dest;
    clock_t start, end;
    double cpu_time_used;
    int i;

    for(i = 0; i < VECTOR_SIZE; i++) {
        arr1[i] = rand() % 9 + 1;
        arr2[i] = rand() % 9 + 1;
    }

    start = clock();
    inner4_unrolled(u, v, &dest);
    end = clock();

    cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;

    printf("%d %1f\n", (int)dest, cpu_time_used);

    return 0;
}

data_t * get_vec_start(vec_ptr v) {

    return v;
}

int vec_length(vec_ptr v) {

    return VECTOR_SIZE;
}

void inner4_unrolled(vec_ptr u, vec_ptr v, data_t *dest) {

    long int i;
    int length = vec_length(u);
    int limit = length - 1;
    data_t *udata = get_vec_start(u);
    data_t *vdata = get_vec_start(v);
    data_t sum = (data_t)0;

    for (i = 0; i < limit; i+=4) {
        sum += udata[i]     * vdata[i];
        sum += udata[i + 1] * vdata[i + 1];
        sum += udata[i + 2] * vdata[i + 2];
        sum += udata[i + 3] * vdata[i + 3];
    }
    for(; i < length; i++) {
        sum = udata[i] * vdata[i];
    }

    *dest = sum;

    return;
}
