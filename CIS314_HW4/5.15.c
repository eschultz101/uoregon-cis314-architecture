//Eric Schultz
//CIS 314
//5.15

//Inner4 procedure test

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define VECTOR_SIZE 100000

typedef long long data_t;
typedef data_t *vec_ptr;

void inner4(vec_ptr u, vec_ptr v, data_t *dest);
int vec_length(vec_ptr v);
data_t * get_vec_start(vec_ptr v);

int main(void) {

    data_t arr1[VECTOR_SIZE];
    data_t arr2[VECTOR_SIZE];
    vec_ptr v = arr1;
    vec_ptr u = arr2;
    data_t dest;
    time_t start, end;
    long double cpu_time_used;
    int i;

    for(i = 0; i < VECTOR_SIZE; i++) {
        arr1[i] = rand() % 9 + 1;
        arr2[i] = rand() % 9 + 1;
    }

    start = clock();
    inner4(u, v, &dest);
    end = clock();

    cpu_time_used = ((end - start));// / CLOCKS_PER_SEC;

    printf("%d %lf %lf %lf\n", (int)dest, (double)start, (double)end, cpu_time_used);

    return 0;
}

data_t * get_vec_start(vec_ptr v) {

    return v;
}

int vec_length(vec_ptr v) {

    return VECTOR_SIZE;
}

void inner4(vec_ptr u, vec_ptr v, data_t *dest) {

    long int i;
    int length = vec_length(u);
    data_t *udata = get_vec_start(u);
    data_t *vdata = get_vec_start(v);
    data_t sum = (data_t)0;

    for (i = 0; i < length; i++) {
        sum = sum + udata[i] * vdata[i];
    }

    *dest = sum;

    return;
}

